import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';


final Set<JavascriptChannel> jsChannels = [
  JavascriptChannel(
      name: 'Print',
      onMessageReceived: (JavascriptMessage message) {
        print(message.message);
      }),
].toSet();


class WebViewPage extends StatelessWidget {
  final String title;
  final String url;

  const WebViewPage({Key key, @required this.url, @required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      url: url,
      javascriptChannels: jsChannels,
      allowFileURLs: true,
      withJavascript: true,
      withLocalUrl: true,
      withOverviewMode: true,
      useWideViewPort: true,
      enableAppScheme: true,
      mediaPlaybackRequiresUserGesture: false,
      withZoom: true,
      withLocalStorage: true,
      hidden: true,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          title,
        ),
      ),
      initialChild: Container(
          child: const Center(
            child: CircularProgressIndicator(),
          )),
    );
  }
}
